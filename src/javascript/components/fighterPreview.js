import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  if(fighter) {    
    const imageElement = createFighterImage(fighter);

    const fighterName = createElement({tagName: 'span',className: 'info'});
    fighterName.innerText = `Name - ${fighter.name}`;

    const fighterHealth = createElement({tagName: 'span',className: 'info'});
    fighterHealth.innerText = `Health - ${fighter.health}`;

    const fighterAttack = createElement({tagName: 'span',className: 'info'});
    fighterAttack.innerText = `Attack - ${fighter.attack}`;

    const fighterDefense = createElement({tagName: 'span',className: 'info'});
    fighterDefense.innerText = `Defense - ${fighter.defense}`;

    fighterElement.append(imageElement, fighterName, fighterHealth, fighterAttack, fighterDefense);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
