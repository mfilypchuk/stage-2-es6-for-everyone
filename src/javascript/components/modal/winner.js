import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  const title = 'Winner!';
  const bodyElement = createWiner(fighter);
  showModal({ title: title, bodyElement: bodyElement });
}
function createWiner(fighter) {
  const imageElement = createFighterImage(fighter);

  const fighterName = createElement({tagName: 'span',className: 'info'});
  fighterName.innerText = `Name - ${fighter.name}`;

  fighterDetails.append(imageElement, nameElement);
  return fighterDetails;
}