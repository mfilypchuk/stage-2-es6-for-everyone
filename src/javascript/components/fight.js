import { controls } from '../../constants/controls';
import { States } from '../../constants/states';

export async function fight(firstFighter, secondFighter) {

  var playerOne = { 
    health: firstFighter.health, 
    attack: firstFighter.attack, 
    defense: firstFighter.defense,
    state: States.None,
    lastCombo :Date.now()
  };

  var playerTwo = { 
    health: secondFighter.health, 
    attack: secondFighter.attack, 
    defense: secondFighter.defense, 
    state: States.None,
    lastCombo : Date.now()
  };
     
  let maxHealthFirstFighter = playerOne.health;
  let maxHealthSecondFighter = playerTwo.health;
  let keysPressed = {};

  document.addEventListener('keydown', (event) => {
    keysPressed[event.code] = true;
    ChangeState(keysPressed, playerOne, playerTwo);
  });
  document.addEventListener('keyup', (event) => {
    delete keysPressed[event.code];
  });

  return new Promise((resolve) => {
    let winer = Actions(playerOne, playerTwo, maxHealthFirstFighter, maxHealthSecondFighter);
    resolve(winer);
    // resolve the promise with the winner when fight is over
  });
}

function Actions(playerOne, playerTwo, maxHealthFirstFighter, maxHealthSecondFighter) {
  var refreshId = setInterval(() => {
      FighterAction(playerTwo, playerOne);
      FighterAction(playerOne, playerTwo);
      FightersIndicator(playerOne.health, playerTwo.health, maxHealthFirstFighter, maxHealthSecondFighter);
      if(playerOne.health <= 0 || playerTwo.health <= 0) {
        clearInterval(refreshId);
      }
  } , 1000)
  if (playerOne.health > playerTwo.health) {
    return playerOne;
  }
  return playerTwo;
}

function ChangeState(keysPressed, playerOne, playerTwo) {
  switch(true) {
    case controls.PlayerOneCriticalHitCombination.every(key => keysPressed[key]):
      playerOne.state = States.Combo;
      console.log("Combo1");
      break;

    case controls.PlayerTwoCriticalHitCombination.every(key => keysPressed[key]):
      playerTwo.state = States.Combo;
      console.log("Combo2");
      break;

    case keysPressed[controls.PlayerOneBlock]:
      playerOne.state = States.Block;
      console.log("Block1");
      break;

    case keysPressed[controls.PlayerTwoBlock]:
      playerTwo.state = States.Block;
      console.log("Block2");
      break;

    case keysPressed[controls.PlayerOneAttack] && keysPressed[controls.PlayerTwoAttack]:
      playerOne.state = States.Attack;
      playerTwo.state = States.Attack;
      console.log("Attack12");
      break; 

    case keysPressed[controls.PlayerOneAttack]:
      playerOne.state = States.Attack;
      console.log("Attack1");
      break; 

    case keysPressed[controls.PlayerTwoAttack]:
      playerTwo.state = States.Attack;
      console.log("Attack2");
      break; 

    default :
      return;       
  }
}

function FighterAction(attacker, defender) {
  switch(attacker.state) {
    case States.None:
      break;
    case States.Block:
      break;
    case States.Attack:
      defender.health -= Attack(attacker, defender);
      break;
    case States.Combo:
      defender.health -= Combo(attacker);
      break;
  }
  return defender.health;
}

function Attack(attacker, defender) {
  attacker.state = States.None
  if(defender.state == States.Block) {    
    return getDamage(attacker, defender);
  }
  return getHitPower(attacker); 
}

function Combo(attacker) {
  attacker.state = States.None
  if(Date.now() - attacker.lastCombo > 10000) {
    return attacker.attack * 2;
  }
}

export function getDamage(attacker, defender) {
  // return damage
  let damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage <= 0) {
      damage = 0;
  }
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = getRandomFromOneToTwo();
  let power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = getRandomFromOneToTwo();
  let power = fighter.defense * dodgeChance;
  return power;
}

function getRandomFromOneToTwo() {
  let random = Math.random() + 1;
  return random;
}

function FightersIndicator(leftHealthFirstFighter, leftHealthSecondFighter, maxHealthFirstFighter, maxHealthSecondFighter) {
  const leftIndicator = document.getElementById('left-fighter-indicator');
  leftIndicator.style.width = leftHealthFirstFighter / maxHealthFirstFighter * 100 + '%';
  if(leftHealthFirstFighter <= 0 ){
    leftIndicator.style.width = '0%'
  }

  const rightIndicator = document.getElementById('right-fighter-indicator');
  rightIndicator.style.width = leftHealthSecondFighter / maxHealthSecondFighter * 100 + '%';
  if(leftHealthSecondFighter <= 0 ){
    rightIndicator.style.width = '0%'
  }
}
