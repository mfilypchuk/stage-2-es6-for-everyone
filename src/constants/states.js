export const States = Object.freeze({
    None:   Symbol("none"),
    Block:  Symbol("block"),
    Attack: Symbol("attack"),
    Combo:  Symbol("combo"),
});
